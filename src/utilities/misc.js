const getCurrentTimestamp = () => {
  return Math.floor(Date.now() / 1000);
};

export {
  getCurrentTimestamp,
};